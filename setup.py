#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from distutils.core import setup

setup(
    name='xivo-dxtorc',
    version='0.1',
    description='XIVO dxtora client',
    maintainer='Avencall',
    maintainer_email='xivo-users@lists.proformatique.com',
    url='http://www.xivo.io/',
    license='GPLv3',
    scripts=['bin/dxtorc'],
)
